const express = require('express');
const app = express();
const mongoose = require('mongoose');
require('dotenv').config({ path: './config/config.env' });
const server = require('http').createServer(app);
const io = require('socket.io')(server);
const CallLogModel = require('./models/CallLog');

const PORT = process.env.PORT || 8080;
app.use(express.json());

const connectDb = async () => {
	await mongoose
		.connect('mongodb://mongo:27017/gcalls', {
			useNewUrlParser: true,
			useUnifiedTopology: true,
		})
		.then(() => console.log('connected to MongoDB'))
		.catch((err) => console.log(err.message));
};

io.on('connection', (socket) => {
	console.log('Connected to the socket server');
	socket.on('createLog', async (callData) => {
		try {
			const newLog = await new CallLogModel(callData);
			await newLog.save();
			socket.emit('createLog', newLog);
		} catch (error) {
			console.log(error.message);
		}
	});

	socket.on('updateLog', async ({ id, dataUpdate }) => {
		try {
			await CallLogModel.findByIdAndUpdate(id, dataUpdate);
		} catch (error) {
			console.log(error.message);
		}
	});

	socket.on('get', async () => {
		try {
			const allLog = await CallLogModel.find({});
			socket.emit('getAllLog', allLog);
		} catch (error) {
			console.log(error.message);
		}
	});

	socket.on('deleteLogById', async (id) => {
		try {
			await CallLogModel.findByIdAndDelete(id);
		} catch (error) {
			console.log(error.message);
		}
	});
});

server.listen(PORT, async () => {
	try {
		await connectDb();
		console.log(`server listening on port ${PORT}`);
	} catch (error) {
		console.log(error.message);
	}
});
